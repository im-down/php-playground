<?php

// check if username or password box is empty
if($_POST[uname] == "" || $_POST[pword] == ""){
	exit("Your username or password is blank!");
}

// hash the info
$hashed_uname = hash("sha256", $_POST[uname]);
$hashed_pword = hash("sha256", $_POST[pword]);

// check if username is already registered
$list = file_get_contents('list.txt');
if(preg_match("/$_POST[uname]/", $list) === 1){
	exit("This username Already Exists!");
}

file_put_contents('list.txt', "\n$hashed_uname\t$hashed_pword", FILE_APPEND);

?>

<br>
<p>you have been registered with usernme: <?php echo $_POST[uname]?></p>
<a href="/pass/">go back</a>

