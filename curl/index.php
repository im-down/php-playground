<?php
if (preg_match("/curl/", $_SERVER['HTTP_USER_AGENT']) === 1){
	echo "Secret Message";
	exit();
}
?>
<h1>cURL example</h1>
<pre>
using the following code:

if (preg_match("/curl/", $_SERVER['HTTP_USER_AGENT']) === 1){
	echo "Secret Message";
	exit();
}

you can embed pretty much anything you want if someone were to acsess this with curl

Try it out! ($ curl http://testing/curl/) (or localhost or wherever you store this repo)
</pre>
